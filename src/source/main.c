#include "stm32f0xx.h"
#include "system_stm32f0xx.h"
#include "main.h"
#include "rcc.h"                //Clock
#include "gpio.h"               //GPIO ports
#include "i2c.h"

#define AT24C256_MAX_BYTE       32768   // 0x8000
#define AT24C256_MAX_PAGE       512     // 0x0200
#define AT24C256_PAGE_SIZE      64      // 0x0040

int main()
{
  uint8_t test[] = "012345678901234567890123456789";

  RCC_HSE_Init();
  RTC_Init();
  
  GPIOA_Init();
  
  I2C_Init();
  
  GPIOA->MODER |= GPIO_MODER_MODER4_0;
  
  while(1)
  {
    I2C_Write(0xA0,test,sizeof(test));
    GPIOA->BSRR = GPIO_BSRR_BR_4;
    for(int i = 0; i < 600000; i++);
    I2C_Read(0xA0,test,sizeof(test));
    GPIOA->BSRR = GPIO_BSRR_BS_4;
    for(int i = 0; i < 600000; i++);
  }
}