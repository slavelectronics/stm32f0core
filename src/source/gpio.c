#include "stm32f0xx.h"
#include "gpio.h"

void GPIOA_Init() {

  RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
  
}

void GPIOB_Init() {

  RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
  
}

void GPIOF_Init() {

  RCC->AHBENR |= RCC_AHBENR_GPIOFEN;

}