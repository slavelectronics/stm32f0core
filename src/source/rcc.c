#include "stm32f0xx.h"
#include "rcc.h"

void RCC_HSE_Init() {
  
  RCC->CR |= ((uint32_t)RCC_CR_HSEON);                  //HSE turned on
  while(!(RCC->CR & RCC_CR_HSERDY));                    //Waiting for HSE ready
  
  FLASH->ACR = FLASH_ACR_PRFTBE | FLASH_ACR_LATENCY;    //Clock Flash memory
  
  RCC->CFGR |= RCC_CFGR_HPRE_DIV1;                      //AHB = SYSCLK/1
  RCC->CFGR |= RCC_CFGR_PPRE_DIV1;                      //APB = HSCLK/1
  
  RCC->CFGR &= ~RCC_CFGR_PLLMULL;                       //Clear PLL multiplier bits
  RCC->CFGR &= ~RCC_CFGR_PLLSRC;                        //Clear PLL source bits
  RCC->CFGR &= ~RCC_CFGR_PLLXTPRE;                      //Clear PLL HSE devider bits
    
  RCC->CFGR |= RCC_CFGR_PLLSRC_PREDIV1;                 //Selecting HSE as clock source and PLL entry devider to 1
  RCC->CFGR |= RCC_CFGR_PLLXTPRE_PREDIV1_Div2;          //Setting HSE divider for PLL input clock (HSE/2 = 4 Mhz)
  RCC->CFGR |= RCC_CFGR_PLLMULL12;                      //Setting PLL multiplier to 8 (PLL = 4*8 = 48 Mhz)
  
  RCC->CR |= RCC_CR_PLLON;                              //PLL turned on
  while ((RCC->CR & RCC_CR_PLLRDY)==0);                 //Waiting for PLL ready
  
  RCC->CFGR &= ~RCC_CFGR_SW;                            //Clear SW bits
  RCC->CFGR |= RCC_CFGR_SW_PLL;                         //Setting source SYSCLK = PLL;
  while((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_1);    //Wait for PLL is used
  
}

void RTC_Init() {
    
  RCC->CSR |= RCC_CSR_LSION;
  while (!(RCC->CSR & RCC_CSR_LSIRDY));

  RCC->APB1ENR |= RCC_APB1ENR_PWREN;
  PWR->CR |= PWR_CR_DBP;
  
  RCC->BDCR |= RCC_BDCR_RTCSEL_LSI;
  RCC->BDCR |= RCC_BDCR_RTCEN;
  
  RTC->WPR = 0xCA;
  RTC->WPR = 0x53;
  
  RTC->ISR |= RTC_ISR_INIT;
  while(!(RTC->ISR & RTC_ISR_INITF));
  
  RTC->PRER = (uint32_t)(99<<16);
  RTC->PRER |= (uint32_t)399;
  
  RTC->ISR &= ~RTC_ISR_INIT;
  
  RTC->WPR = 0xFF;
  
}

void RTC_Set (RTC_Struct *value) {

  uint32_t TR, DR;
  
  RTC->WPR = 0xCA;
  RTC->WPR = 0X53;
  
  RTC->ISR |= RTC_ISR_INIT;
  while(!(RTC->ISR & RTC_ISR_INITF));
  
  TR = (((uint32_t)(value->hour) << 16) | ((uint32_t)(value->minute)) << 8 | ((uint32_t)(value->second)));
  RTC->TR = TR & RTC_TR_MASK;
  
  DR = (((uint32_t)(value->year) << 16) | ((uint32_t)(value->week) << 13) | ((uint32_t)(value->mounth)) | ((uint32_t)(value->day)));
  RTC->DR = DR & RTC_DR_MASK;
  
  RTC->ISR &= ~RTC_ISR_INIT;
  
  RTC->WPR = 0xFF;
  
}
void RTC_Start (RTC_Struct *value) {
  
  uint32_t TR,DR;
  
  RCC->CSR |= RCC_CSR_LSION;
  while(!(RCC->CSR & RCC_CSR_LSIRDY));
  
  RCC->APB1ENR |= RCC_APB1ENR_PWREN;
  PWR->CR |= PWR_CR_DBP;
  
  if (!(RTC->ISR & RTC_ISR_INITS)) {
    
    RCC->BDCR |= RCC_BDCR_RTCSEL_LSI;
    RCC->BDCR |= RCC_BDCR_RTCEN;
    
    RTC->WPR = 0xCA;
    RTC->WPR = 0x53;
    
    RTC->ISR |= RTC_ISR_INIT;
    while(!(RTC->ISR & RTC_ISR_INITF));
    
    RTC->PRER = (uint32_t)(99 << 16);
    RTC->PRER |= (uint32_t)399;
    
    TR = (((uint32_t)(value->hour) << 16) | ((uint32_t)(value->minute)) << 8 | ((uint32_t)(value->second)));
    RTC->TR = TR & RTC_TR_MASK;
    
    DR = (((uint32_t)(value->year) << 16) | ((uint32_t)(value->week) << 13) | ((uint32_t)(value->mounth)) | ((uint32_t)(value->day)));
    RTC->DR = DR & RTC_DR_MASK;
    
    RTC->ISR &= ~RTC_ISR_INIT;
    RTC->WPR = 0xFF;
    
  }
  
}

void RTC_Get (RTC_Struct *value) {

  uint32_t TR, DR;
  
  TR = RTC->TR;
  DR = RTC->DR;
  
  value->hour = (uint8_t)(TR >> 16) & (uint8_t)0x3F;
  value->minute = (uint8_t)(TR >>8) & (uint8_t)0x7F;
  value->second = (uint8_t)(TR) & (uint8_t)0x7F;
  
  value->year = (uint8_t)(DR >>16);
  value->week = (uint8_t)(DR >> 13) & (uint8_t)0x7;
  value->mounth = (uint8_t)(DR >> 8) & (uint8_t)0x1F;
  value->day = (uint8_t)(DR) & (uint8_t)0x3F;
  
}

void RTC_Alarm (RTC_Struct *value, uint8_t msk) {
  
  uint32_t alr;
  
  NVIC_EnableIRQ(RTC_IRQn);
  EXTI->IMR |= EXTI_IMR_MR17;
  EXTI->IMR |= EXTI_RTSR_TR17;
   
  RTC->WPR = 0xCA;
  RTC->WPR = 0x53;
  
  RTC->CR |= RTC_CR_ALRAIE;
  
  RTC->CR &= ~RTC_CR_ALRAE;
  while(!(RTC->ISR & RTC_ISR_ALRAWF));
  
  RTC->ALRMAR = ((msk & 0x01) << 7);
  RTC->ALRMAR = ((msk & 0x02) << 14);
  RTC->ALRMAR = ((msk & 0x04) << 21);
  RTC->ALRMAR = ((msk & 0x08) << 28);
  
  alr = ((uint32_t)(value->day) << 24) | ((uint32_t)(value->hour) << 16) | ((uint32_t)(value->minute) << 8) | ((uint32_t)(value->second));
  RTC->ALRMAR |= alr;
  
  RTC->CR |= RTC_CR_ALRAE;
  RTC->WPR = 0xFF;
  
}

void RTC_IRQHandler () {
  
  if(RTC->ISR & RTC_ISR_ALRAF) {
    GPIOA->ODR ^= GPIO_ODR_6;
    
    RTC->ISR &= ~RTC_ISR_ALRAF;
    EXTI->PR |= EXTI_PR_PR7;
  }
  
}

uint8_t RTC_ByteToBCD(uint8_t Value) {
  
  uint8_t BCD_High = 0;
  while (Value >= 10) {
    BCD_High++;
    Value -= 10;
  }
  return ((uint8_t)(BCD_High << 4) | Value);
  
}

uint8_t RTC_BCDToByte(uint8_t Value) {
  
    uint8_t temp = 0;
    temp = ((uint8_t)(Value & (uint8_t)0xF0) >> (uint8_t)0x4)*10;
    return(temp + (Value & (uint8_t)0x0F));
    
}

uint8_t RTC_BCD_Elder(uint8_t value) {
    
  return (value >> 4);
  
}

uint8_t RTC_BCD_Under(uint8_t value) {

  return (value & (uint8_t)0x0F);  
  
}