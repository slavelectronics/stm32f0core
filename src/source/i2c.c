#include "stm32f0xx.h"
#include "i2c.h"

/*-------------------------*/
/*   I2C1 INITIALISATION   */
/*-------------------------*/

void I2C_Init() {

  RCC->AHBENR |= RCC_AHBENR_GPIOAEN;    //Enable port
  RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;   //
  
  GPIOA->MODER |= GPIO_MODER_MODER9_1;  //Set pin PA9 as SDA
  GPIOA->OTYPER |= GPIO_OTYPER_OT_9;    //Set output type Open-drain
  GPIOA->AFR[1] |= (0x04<<(9-8)*4);     //AF4 << (PA9(b.9>7) - 8)*4)
  
  GPIOA->MODER |= GPIO_MODER_MODER10_1; //Set pin PA10 as SCL
  GPIOA->OTYPER |= GPIO_OTYPER_OT_10;   //Set output type Open-drain
  GPIOA->AFR[1] |= (0x04<<(10-8)*4);    //AF4 << (PA10(b.10>7) - 8)*4
  

  I2C1->OAR2 &= ~I2C_OAR2_OA2EN;        //Own address disabled
  I2C1->CR1 &= ~I2C_CR1_GCEN;           //General call disabled
  I2C1->CR1 &= ~I2C_CR1_NOSTRETCH;      //Clock stretching enabled
  
  I2C1->CR1 &= ~I2C_CR1_PE;             //Peripheral disabled
  
  I2C1->CR1 |= I2C_CR1_ANFOFF;          //Analog noise filter disabled
  I2C1->CR1 |= I2C_CR1_DNF;             //Digital noise filter enabled
  
  I2C1->TIMINGR |= (uint32_t)0x00B01A4B;//Timig cofigured to 400kHz, with I2CCLK = 48MHz, rise time = 140ns, fall time = 40ns
  
  I2C1->CR1 |= I2C_CR1_PE;              //Peripheral enabled      
//  I2C1->CR1 |= I2C_CR2_AUTOEND | (1<<16) | (0x5A << 1);
  
}

/*----------------*/
/*   I2C1 WRITE   */
/*----------------*/

void I2C_Write(uint16_t device, uint8_t *buf,uint8_t size) {
  
  uint8_t i = 0;
  size--;
  
  I2C1->CR2 &= ~I2C_CR2_RD_WRN;         //Set transfer direction (Master requests a write transfer)
  I2C1->CR2 &= ~I2C_CR2_ADD10;          //Set 7-bit addressing mode
  I2C1->CR2 |= (uint32_t)size<<16;      //Write number of bytes to be transmetted to NBYTES[7:0] bits of CR2 register

  
  I2C1->CR2 |= (uint32_t)device;        //Write 7-bit address to SADD[7:1] bits of CR2 register
  I2C1->CR2 |= I2C_CR2_START;           //START generetion
  
  while(!(I2C1->ISR & I2C_ISR_BUSY));   //Wait while START condition is detected
  
  while(i<size){                        //Writing cycle
    I2C1->TXDR = (uint8_t)(buf[i]);     //Write bytes to 8-bit Transmit Data Register
    while(!(I2C1->ISR & I2C_ISR_TXE));  //Wait while Transmit Data Register is empty
    i++;                                //Go to next byte
  }
  
  I2C1->CR2 |= I2C_CR2_STOP;            //STOP generation 
}

/*---------------*/
/*   I2C1 READ   */
/*---------------*/

void I2C_Read(uint16_t device, uint8_t* buf, uint8_t size) {
  
  size--;
  uint8_t i = 0;
  
  I2C1->CR2 &= ~I2C_CR2_RD_WRN;
  I2C1->CR2 |= (uint32_t)size<<16; 
  I2C1->CR2 &= ~I2C_CR2_ADD10;
  
  I2C1->CR2 |= (uint32_t)device;  
  I2C1->CR2 |= I2C_CR2_START;
  
  while(!(I2C1->ISR & I2C_ISR_BUSY));
  I2C1->TXDR = buf[0];
  while(!(I2C1->ISR & I2C_ISR_TXE));
  I2C1->TXDR = buf[1]; 
}


























