#ifndef I2C_H
#define I2C_H

void I2C_Init (void);
void I2C_Write(uint16_t device, uint8_t *buf, uint8_t size);
void I2C_Read (uint16_t device, uint8_t* buf, uint8_t size);
#endif /*i2c.h*/