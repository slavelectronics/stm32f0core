#ifndef GPIO_H
#define GPIO_H

void GPIOA_Init(void);
void GPIOB_Init(void);
void GPIOF_Init(void);

#endif /*gpio.h*/