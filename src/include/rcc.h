#ifndef RCC_H
#define RCC_H

#define RTC_TR_MASK 0x007F7F7F
#define RTC_DR_MASK 0x00FFFF3F
#define RTC_SET 0x01

typedef struct {
    
  uint8_t year;
  uint8_t mounth;
  uint8_t week;
  uint8_t day;
  uint8_t hour;
  uint8_t minute;
  uint8_t second;
    
} RTC_Struct;

void RCC_HSE_Init(void);

void RTC_Init(void);
void RTC_Set(RTC_Struct *value);
void RTC_Start(RTC_Struct *value);
void RTC_Get(RTC_Struct *value);
void RTC_Alarm (RTC_Struct *value, uint8_t msk);

void RTC_IRQHandler (void);

uint8_t RTC_ByteToBCD(uint8_t Value);
uint8_t RTC_BCDToByte(uint8_t Value);
uint8_t RTC_BCD_Elder(uint8_t value);
uint8_t RTC_BCD_Under(uint8_t value);

#endif /*rcc.h*/